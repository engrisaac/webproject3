from django.urls import path , include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('school' ,views.SchoolView)
router.register('student', views.StudentView)

urlpatterns = [
   path('', include(router.urls)),
   path('', include(router.urls))
]
