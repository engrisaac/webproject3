from django.shortcuts import render
from rest_framework import viewsets
from .models import School
from .models import Student
from . import serializers
from . import models

from .serializers import SchoolSerializer
from .serializers import StudentSerializer


class SchoolView(viewsets.ModelViewSet):
    queryset = School.objects.all()
    serializer_class = serializers.SchoolSerializer
    queryset = models.School.objects.all()
  
    def  put(self , request , *args, **kwargs):
       return self.partial_update(request , *args ,**kwargs)


class StudentView(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = serializers.StudentSerializer    
    queryset = models.Student.objects.all()

    def  put(self , request , *args, **kwargs):
       return self.partial_update(request , *args ,**kwargs)