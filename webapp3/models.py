from django.db import models

class School(models.Model):
    name = models.CharField(max_length = 20)
    location = models.CharField(max_length = 40)


    def __str__(self):
       return " name : {} => location : {}".format(self.name , self.location) 



class Student(models.Model):
    name = models.CharField(max_length = 25)
    class_level = models.CharField(max_length = 10)
    course_code = models.CharField(max_length = 5)
    student_level = models.CharField(max_length = 5)
 



    def __str__(self):

        return " name : {} => class_level : {} => course_code : {} => student_level : {}".format(self.name,
          self.class_level , self.course_code , self.student_level)


