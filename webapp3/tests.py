from tastypie.test import ResourceTestCase
from .models import School
from .models import Student


class EntryResourceTest(ResourceTestCase):
    
    def test_api_json(self):
        resp = self.api_client.get('/api/school', format ='json')
        self.assertValidJSONResponse(resp)