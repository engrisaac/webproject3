from rest_framework import serializers
from .models import School
from .models import Student



class SchoolSerializer(serializers.ModelSerializer):
      class Meta:
          model = School
          fields = ( 'id','name', 'location')






class  StudentSerializer(serializers.ModelSerializer):
      class Meta:
          model = Student
          fields = ('id','name' , 'class_level' , 'course_code', 'student_level')